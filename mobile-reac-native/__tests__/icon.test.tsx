/**
 * @format
 */

import renderer from 'react-test-renderer';
import {Icon} from '../src/components';
import {Colors} from '../src/common';
import * as React from 'react';

test('Icon is rendering', () => {
  const tree = renderer
    .create(<Icon size={20} color={Colors.black} name={'search-outline'} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
