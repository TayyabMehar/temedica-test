/**
 * @format
 */

import {filteredDataFunc} from '../src/screens/home/home';
import data from '../src/screens/home/dataset.json';

test('Filter Data Function', () => {
  const value = filteredDataFunc(data, 'audi');
  expect(value.length).toBe(3);
});
