/**
 * @format
 */

import renderer from 'react-test-renderer';
import * as React from 'react';
import Header from '../src/screens/home/header';

test('Header is rendering', () => {
  const tree = renderer.create(<Header />).toJSON();
  expect(tree).toMatchSnapshot();
});
