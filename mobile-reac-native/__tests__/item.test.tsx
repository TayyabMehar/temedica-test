/**
 * @format
 */

import renderer from 'react-test-renderer';
import * as React from 'react';
import Item from '../src/screens/home/item';
import dataSet from '../src/screens/home/dataset.json';

test('Item is rendering', () => {
  const tree = renderer.create(<Item item={dataSet.drugs[0]} />).toJSON();
  expect(tree).toMatchSnapshot();
});
