/**
 * @format
 */

import renderer from 'react-test-renderer';
import {Text} from '../src/components';
import * as React from 'react';

test('Text is rendering', () => {
  const tree = renderer.create(<Text>{'sample text'}</Text>).toJSON();
  expect(tree).toMatchSnapshot();
});
