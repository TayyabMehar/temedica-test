#React Native

`Commands`

Go to your project's root folder and run `npm install`

`npm test`

To run the different tests written.

`cd ios/` and run `pod install`

Run `npm run ios` or `npm run android` to start your application!

Build Android

Run `npm run build:debug-apk`

`Things Covered`
~~~~
TypeScrpit
i18n
Redux
Styled Components
Navigation
Jest
Components, Hooks, State, Props etc
Fonts, Colors in Global
Custom Font Family
~~~~
