import * as React from 'react';
import {TextProps} from 'react-native';
import styled, {css} from 'styled-components/native';
import {Colors, Constants} from '../../common';
const {fontSize, fontFamily} = Constants;

export type LabelCustomProps = {
  size?: number;
  family?: string;
  color?: string;
  children: string;
};

export type LabelProps = LabelCustomProps & TextProps;

const StyledText = styled.Text<Pick<LabelProps, 'color' | 'family' | 'size'>>`
  ${({color, family, size}: LabelCustomProps) => css`
    font-family: ${family || fontFamily.LatoRegular};
    font-size: ${size || fontSize.small}px;
    color: ${color || Colors.black};
  `}
`;

export function Text({children, ...rest}: LabelProps) {
  return <StyledText {...rest}>{children}</StyledText>;
}
