import * as React from 'react';
import {TextInputProps, TextProps, ViewProps} from 'react-native';
import styled, {css} from 'styled-components/native';
import {Colors, Constants} from '../../common';
import {Icon} from '..';
import {ClearSearchFunc} from '../../screens/home/searchInput';
import {canBeCleared} from '../../helpers/common.helpers';
const {fontSize, fontFamily} = Constants;

const StyledContainer = styled.View<ViewProps>`
  width: 100%;
`;

const StyledText = styled.Text<TextProps>`
  margin-bottom: 10px;
  ${() => css`
    font-family: ${fontFamily.LatoMedium};
    font-size: ${fontSize.normal}px;
    color: ${Colors.black};
  `}
`;

const StyledSearch = styled.View<ViewProps>`
  width: 100%;
  flex-direction: row;
  align-items: center;
  padding: 0px 10px;
  ${() => css`
    background-color: ${Colors.lightGrey};
    font-size: ${fontSize.normal}px;
    color: ${Colors.black};
  `}
`;

const StyledInput = styled.TextInput<TextInputProps>`
  flex: 1;
  padding-horizontal: 10px;
  ${() => css`
    font-family: ${fontFamily.LatoRegular};
    font-size: ${fontSize.small}px;
    color: ${Colors.black};
  `}
`;

export type InputFieldProps = {
  heading: string;
  value: string;
  clearSearch: ClearSearchFunc;
} & TextInputProps;

export function SearchBar(props: InputFieldProps) {
  return (
    <StyledContainer>
      <StyledText>{props.heading}</StyledText>
      <StyledSearch>
        <Icon size={20} color={Colors.black} name={'search-outline'} />
        <StyledInput {...props} />
        {Boolean(canBeCleared(props?.value)) && (
          <Icon
            size={20}
            color={Colors.black}
            name={'close-outline'}
            onPress={props.clearSearch}
          />
        )}
      </StyledSearch>
    </StyledContainer>
  );
}
