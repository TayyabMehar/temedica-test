import * as React from 'react';
import {ViewStyle} from 'react-native';
import iconIconsJson from 'react-native-vector-icons/glyphmaps/Ionicons.json';
import RNVIcon from 'react-native-vector-icons/Ionicons';
import styled, {css} from 'styled-components/native';

type IconName = keyof typeof iconIconsJson;

const StyledRNVIcon = styled(RNVIcon)<IconProps>`
  text-align: center;
  ${({size}: {size: number}) =>
    size
      ? css`
          width: ${size}px;
          height: ${size}px;
        `
      : null}
`;

export type IconProps = {
  name: IconName;
  size?: number;
  color?: string;
  style?: ViewStyle;
  onPress?: any;
};

export function Icon({name, size, color, style, ...rest}: IconProps) {
  return (
    <StyledRNVIcon
      name={name}
      size={size}
      color={color}
      style={style}
      {...rest}
    />
  );
}
