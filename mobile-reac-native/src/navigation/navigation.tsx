import * as React from 'react';
import {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {StatusBar, Platform, LogBox} from 'react-native';
import styled from 'styled-components/native';

import Home from '../screens/home/home';
import {Colors} from '../common';
import translations from '../assets/translations.json';
import {setI18nTranslations} from '../utils';
import {useState} from 'react';
import {SafeAreaViewProps} from 'react-native-safe-area-context';
const MainStack = createStackNavigator();

const Navigation = () => {
  LogBox.ignoreAllLogs();
  const [appState, setAppState] = useState(false);
  useEffect(() => {
    setI18nTranslations(translations);
    setAppState(true);
  }, []);

  // it takes some time to load translations
  if (!appState) {
    return null;
  }

  const StyledSafeArea = styled.SafeAreaView<SafeAreaViewProps>`
    flex: 1;
  `;

  return (
    <StyledSafeArea>
      <StatusBar
        hidden={false}
        backgroundColor={Colors.black}
        barStyle={Platform.OS == 'ios' ? 'dark-content' : 'light-content'}
      />
      <NavigationContainer>
        <MainStack.Navigator>
          <MainStack.Screen
            name="Home"
            component={Home}
            options={{
              headerShown: false,
              gestureEnabled: false,
            }}
          />
        </MainStack.Navigator>
      </NavigationContainer>
    </StyledSafeArea>
  );
};

export default Navigation;
