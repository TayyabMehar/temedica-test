//Node imports
import React, {useState} from 'react';
import {ViewProps} from 'react-native';

//Local imports
import Header from './header';
import SearchInput from './searchInput';
import styled, {css} from 'styled-components/native';
import {Colors} from '../../common';
import ListItems from './listItems';
import dataSet from './dataset.json';

export type DrugDataType = {
  id: string;
  diseases: string[];
  description: string;
  name: string;
  released: string;
};

const StyledContainer = styled.View<ViewProps>`
  flex: 1;
  width: 100%;
  padding-horizontal: 10px;
  padding-vertical: 20px;
  ${() => css`
    background-color: ${Colors.white};
  `}
`;

export const filteredDataFunc = (
  data: {drugs: DrugDataType[]},
  text: string,
) => {
  return data.drugs.filter(
    (obj: DrugDataType) =>
      obj?.name.toLowerCase().includes(text) ||
      obj?.description.toLowerCase().includes(text) ||
      obj?.diseases.filter(disease => disease.toLowerCase().includes(text))
        .length > 0,
  );
};

const Home = () => {
  const [drugsList, setDrugsList] = useState<DrugDataType[]>([]);

  const filterSearchData = async (text: string) => {
    if (text.length == 0 && drugsList.length > 0) {
      setDrugsList([]);
    } else if (text.length >= 2) {
      setDrugsList(filteredDataFunc(dataSet, text.toLowerCase()));
    }
  };

  return (
    <StyledContainer>
      <Header />
      <SearchInput filterSearchData={filterSearchData} />
      <ListItems drugsList={drugsList} />
    </StyledContainer>
  );
};

export default Home;
