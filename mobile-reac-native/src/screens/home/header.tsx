//Node imports
import React from 'react';
import {ViewProps, ImageProps} from 'react-native';
import styled from 'styled-components/native';

//Local imports
import {Images} from '../../common';

const StyledContainer = styled.View<ViewProps>`
  width: 100%;
  align-items: center;
  margin-vertical: 15px;
`;

const StyledImage = styled.Image<ImageProps>`
  width: 80px;
  height: 80px;
`;

const Header = () => {
  return (
    <StyledContainer>
      <StyledImage source={Images.logo_image} />
    </StyledContainer>
  );
};

export default Header;
