//Node imports
import React from 'react';
import {ViewProps, TextProps} from 'react-native';

//Local imports
import styled, {css} from 'styled-components/native';
import {Colors, Constants} from '../../common';
import {Text} from '../../components';
import {DrugDataType} from './home';
const {fontSize, fontFamily} = Constants;

const Item = ({item}: {item: DrugDataType}) => {
  const StyledListOuter = styled.View<ViewProps>`
    padding: 10px;
    margin-vertical: 5px;
    box-shadow: 10px 5px 5px black;
    ${() => css`
      border: 1px solid ${Colors.lightGrey};
    `}
  `;

  const StyledRow = styled.View<ViewProps>`
    flex-direction: row;
    justify-content-: space-between;
  `;

  const StyledDiseaseOuter = styled.View<ViewProps>`
    padding-vertical: 15px;
  `;

  return (
    <StyledListOuter>
      <StyledRow>
        <Text family={fontFamily.LatoBold}>{item.name}</Text>
        <Text>{item.released}</Text>
      </StyledRow>
      <StyledDiseaseOuter>
        {item.diseases.map((disease, index) => (
          <Text size={fontSize.tinyM} key={'disease_' + item.id + index}>
            {disease}
          </Text>
        ))}
      </StyledDiseaseOuter>
      <Text>{item.description}</Text>
    </StyledListOuter>
  );
};

export default Item;
