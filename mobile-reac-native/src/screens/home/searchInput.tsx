//Node imports
import React, {useState} from 'react';
import {ViewProps} from 'react-native';

//Local imports
import {Colors} from '../../common';
import {SearchBar} from '../../components';
import {t} from 'i18n-js';
import styled from 'styled-components/native';

export type TextChangeFunc = (str: string) => void;
export type ClearSearchFunc = () => void;
export type SearchBarPropsType = {
  filterSearchData: TextChangeFunc;
};

const StyledContainer = styled.View<ViewProps>`
  width: 100%;
  margin-bottom: 30px;
`;

const SearchInput = (props: SearchBarPropsType) => {
  const {filterSearchData} = props;
  const [searchValue, setSearchValue] = useState('');

  const onSearchTextChange = (text: string) => {
    setSearchValue(text);
    filterSearchData(text);
  };

  const clearSearch = () => {
    setSearchValue('');
    filterSearchData('');
  };

  return (
    <StyledContainer>
      <SearchBar
        heading={t('SEARCH')}
        placeholder={t('SEARCH_TERM')}
        placeholderTextColor={Colors.black}
        onChangeText={onSearchTextChange}
        value={searchValue}
        clearSearch={clearSearch}
      />
    </StyledContainer>
  );
};

export default SearchInput;
