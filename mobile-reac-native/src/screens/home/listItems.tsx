//Node imports
import React from 'react';
import {ViewProps, FlatList} from 'react-native';

//Local imports
import {t} from 'i18n-js';
import styled, {css} from 'styled-components/native';
import {Colors} from '../../common';
import Item from './item';
import {DrugDataType} from './home';
import {Text} from '../../components';

const StyleListHeader = styled.View<ViewProps>`
  padding-bottom: 15px;
  ${() => css`
    background-color: ${Colors.white};
  `}
`;

const ListItems = props => {
  const {drugsList} = props;

  const StyledFlatList = styled(
    FlatList as new () => FlatList<{name: string}>,
  )``;

  const renderListItem = ({item}: {item: DrugDataType}) => <Item item={item} />;

  return (
    <StyledFlatList
      stickyHeaderIndices={[0]}
      ListHeaderComponent={
        drugsList?.length > 0 && (
          <StyleListHeader>
            <Text>{t('SHOWING_X_RESULTS', {value: drugsList.length})}</Text>
          </StyleListHeader>
        )
      }
      data={drugsList}
      renderItem={renderListItem}
      keyExtractor={(item: DrugDataType) => item.id}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
    />
  );
};

export default ListItems;
