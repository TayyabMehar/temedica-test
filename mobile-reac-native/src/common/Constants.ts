import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const Constants = {
  fontSize: {
    sTiny: 10,
    tinyM: 12,
    tiny: 13,
    small: 14,
    normal: 16,
    medium: 18,
    large: 20,
    xLarge: 22,
    huge: 30,
  },

  Dimension: {
    ScreenWidth(percent = 1) {
      return Dimensions.get('window').width * percent;
    },
    ScreenHeight(percent = 1) {
      return Dimensions.get('window').height * percent;
    },
  },

  Window: {
    width,
    height,
  },

  fontFamily: {
    LatoThin: 'Lato-Thin',
    LatoLight: 'Lato-Light',
    LatoRegular: 'Lato-Regular',
    LatoMedium: 'Lato-Medium',
    LatoBold: 'Lato-Bold',
    LatoExtraBold: 'Lato-Black',
  },

  defaultLocale: 'en',
};
export default Constants;
