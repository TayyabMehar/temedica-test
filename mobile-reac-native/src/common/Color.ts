export default {
  transparent: 'transparent',

  //White
  white: '#FFFFFF',

  //Grey
  lightGrey: '#dcdcdc',
  greyFont: '#3c4856',

  //Black
  black: '#000000',
  greyDBlack: '#1D1D1D',

  // blue
  packageBackground: 'rgba(10, 161, 244, 0.1)',
  deepskyblue: '#0aa1f4',
  dodgerblue: 'rgb(30,144,255)',

  // Cyan green
  cyanGreen: 'rgba(0, 180, 151, 1)',
  cyanGreen01: 'rgba(0, 180, 151, 0.1)',

  // Orange
  burningOrange: '#f96f4b',
  burningOrange01: 'rgba(249,111,75, 0.1)',
};
