import i18n from 'i18n-js';
import {NativeModules, Platform} from 'react-native';
import {Constants} from '../common';

i18n.defaultLocale = Constants.defaultLocale;
i18n.fallbacks = true;

const getDeviceLanguageCode = () => {
  const languageCode =
    Platform.OS === 'ios'
      ? NativeModules.SettingsManager.settings.AppleLocale ||
        NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
      : NativeModules.I18nManager.localeIdentifier;
  return languageCode.split('_')[0];
};

export function setI18nTranslations(translations: {
  [locale: string]: Record<string, string>;
}) {
  i18n.locale = getDeviceLanguageCode() || Constants.defaultLocale;
  i18n.translations = translations;
}
