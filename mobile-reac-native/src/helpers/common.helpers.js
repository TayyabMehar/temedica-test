import {Language} from '../common';

export const canBeCleared = field => field && field.length > 0;
export const isEmpty = field => !field || field.length == 0;
export const lessLength = (field, len) => !field || field.length < len;
export const notEqual = (field, len) => !field || field.length != len;
export const parseApiError = err => {
  if (typeof err === 'string') {
    return err;
  }
  let keys = Object.keys(err);
  if (keys.length > 0) {
    let errorString = '';
    for (let i = 0; i < keys.length; i++) {
      let currentError = Array.isArray(err[keys[i]])
        ? keys[i] + ':' + err[keys[i]][0]
        : err[keys[i]];
      errorString = errorString + currentError;
      if (i !== keys.length - 1) {
        errorString += '\n';
      }
    }
    return errorString;
  } else {
    return Language.Constants.someThingWentWrong;
  }
};
