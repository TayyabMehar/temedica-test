// Importing: Dependencies
import {combineReducers} from 'redux';
// Importing: Reducers
import * as drugs from './drugsReducer';

// Redux: Root Reducer
const rootReducer = combineReducers(Object.assign(drugs));
// Exports
export default rootReducer;
