import types from '../actions/types';
import {DrugDataType} from '../../screens/home/home';

export type DrugReducerType = {
  status: boolean | null;
  message: string | null;
  error: string | null;
  loading: boolean;
  data: DrugDataType[];
};

const initialState: DrugReducerType = {
  status: null,
  message: null,
  error: null,
  data: [],
  loading: false,
};

const requestState = {
  ...initialState,
  loading: true,
};

export const drugs = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_ALL_DRUGS_REQUEST:
      return {
        ...state,
        ...requestState,
      };
    case types.GET_ALL_DRUGS_SUCCESS:
      return {
        ...state,
        status: action.payload.status,
        message: action.payload.message,
        error: null,
        data: action.payload.data,
        loading: false,
      };
    case types.GET_ALL_DRUGS_FAILURE:
      return {
        ...state,
        status: false,
        message: action.error.message || '',
        error: action.error.data || action.error,
        data: null,
        loading: false,
      };
    default:
      return state;
  }
};
