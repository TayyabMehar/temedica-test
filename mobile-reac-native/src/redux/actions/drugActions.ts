import types from './types';
// GET ALL DRUGS
export const getDrugsList = (params: string) => {
  return {
    type: types.GET_ALL_DRUGS_REQUEST,
    payload: params,
  };
};
