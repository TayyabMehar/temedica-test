import {HOST} from './urls';

class Api {
  static headers(multipart = false) {
    if (multipart) {
      return {
        'Content-Type': 'multipart/form-data',
      };
    } else {
      return {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      };
    }
  }

  static get(route: string, sendAuthToken = false) {
    return this.xhr(route, null, 'GET', sendAuthToken, false);
  }

  static put(route: string, params: any, sendAuthToken = false) {
    return this.xhr(route, params, 'PUT', sendAuthToken, false);
  }

  static post(
    route: string,
    params: any,
    sendAuthToken = false,
    multipart = false,
  ) {
    console.log('Route:::', route, 'Data:::', params);
    return this.xhr(route, params, 'POST', sendAuthToken, multipart);
  }

  static delete(route: string, params: any, sendAuthToken = false) {
    return this.xhr(route, params, 'DELETE', sendAuthToken, false);
  }

  static async xhr(
    route: string,
    params: any,
    verb: string,
    sendAuthToken: boolean,
    multipart: boolean,
  ) {
    console.log(route, params, verb, sendAuthToken, multipart);
    const url = `${HOST}/${route}`;
    let options = Object.assign(
      {method: verb},
      params ? {body: multipart ? params : JSON.stringify(params)} : null,
    );

    console.log('url::', url);
    console.log('Options::', options);

    return fetch(url, options)
      .then(resp => {
        let json = resp.json();
        // console.log("Response::", json);
        console.log('Resp::', resp.ok, resp.status);
        if (resp.ok) {
          return json;
        }
        return json.then(err => {
          console.log('Err::', err);
          throw err;
        });
      })
      .then(res => {
        return res;
      });
  }
}
export default Api;
