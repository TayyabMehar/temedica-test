import type {Config} from '@jest/types';

const config: Config.InitialOptions = {
  testEnvironment: 'jsdom',
  globalSetup: '<rootDir>/jest.global-setup.ts',
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  preset: 'react-native',
  transformIgnorePatterns: [
    'node_modules/(?!(react-native' +
      '|@react-native' +
      '|@react-navigation' +
      '|react-native-vector-icons' +
      ')/)',
  ],
};

export default config;
