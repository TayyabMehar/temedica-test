'use strict';
const drugModel = require('../models/drugModel');
const appConstants = require('../constants/common');
const commonFunctions = require('../helpers/commonFunctions');
const dataSet = require('../constants/dataset');

async function createOnFirstTime() {
    try {
        let allRecords = await drugModel.find();
        if(allRecords?.length == 0) await drugModel.insertMany(dataSet.drugs);
    } catch (e) {
        commonFunctions.consoleLog('unable to create records');
    }
}
createOnFirstTime();

const getAllDrugs = async function (req, res) {
    try {
        const reqData = commonFunctions.getReqParams(req);
        let filter = {};
        // WE CAN USE BOTH TEXT AND REGEX Search just to comment in/out relevant line of code below

        // if(reqData.filter) filter = {$text: {$search: reqData.filter}}, {score: {$meta: "textScore"}};
        if(reqData.filter) filter = {$or: [
                {"name": { "$regex": reqData.filter, "$options": "i" }},
                {"diseases": { "$regex": reqData.filter, "$options": "i" }},
                {"description": { "$regex": reqData.filter, "$options": "i" }},
            ]};
        const allRecords = await drugModel.find(filter);
        return commonFunctions.sendResponse(res, appConstants.CODE.SUCCESS, appConstants.RESPONSE_MESSAGES.SUCCESS.DRUGS_LIST, allRecords)
    } catch (e) {
        return commonFunctions.sendResponse(res, e.code, e.message)
    }
}

module.exports = {
    getAll: getAllDrugs
}
