#Node Js

`Commands`

Go to your project's root folder and run `npm install`

`npm start`

To run the server with `development` configurations on PORT 3001.

`npm run prod`

To run the server with `production` configurations  on PORT 3001.


I have created `index for text search` for all string fields in schema and have also implemented 
`regex search` to search for partial words. 

System will insert all the records for first time in database if not there already and then
 will search from it.
 
` MongoDB |
 Express.Js |
 Mongoose | Node Js`
