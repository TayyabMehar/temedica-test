'use strict'
const appConstants = require('../constants/common');
const config = require('../config/config.js');

const sendResponse = (res, code, message = null, data = null) => {
    res.status(getHTTPStatusCode(code)).json({status: getStatusTrueFalse(code), message, data});
};

const getHTTPStatusCode = (code) => {
    if (code == appConstants.CODE.SUCCESS || code == appConstants.CODE.NOT_FOUND || code == appConstants.CODE.UNAUTHORIZED
        || code == appConstants.CODE.BAD_REQUEST) return code;
        return appConstants.CODE.SERVER_ERROR;
};

const getStatusTrueFalse = (code) => {
    return code == appConstants.CODE.SUCCESS;
};

const consoleLog = (message, data) => {
    console.log(message, ' ==> ', data);
}

const getReqParams = (req) => {
    return checkObjectNotEmpty(req.body) ? req.body : checkObjectNotEmpty(req.params) ? req.params : req.query;
}

const checkObjectNotEmpty = (obj) => {
    return Object.entries(obj).length > 0;
}

module.exports = {
    sendResponse: sendResponse,
    getReqParams: getReqParams,
    consoleLog: consoleLog,
}
