const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const db = require('./config/database');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./public/swagger.yaml');
const bodyParser = require('body-parser');

const app = express();

// call the database connectivity function
db();

// var connection = mongoose.connection;

app.use(cors());

// Routes
const indexRouter = require('./routes/index');
const drugRoutes = require('./routes/drugRoutes');


// swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// for parsing application/json
app.use(bodyParser.json({limit: '50mb', extended: true, parameterLimit: 100000,}));

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 100000, }));
//form-urlencoded
// for parsing multipart/form-data


app.use('/', indexRouter);
app.use('/drugs', drugRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
