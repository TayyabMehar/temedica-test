'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const db = mongoose.connection;

const drug = new Schema({
    name: {
        type: String,
        required: true
    },
    diseases: [String],
    description: {
        type: String,
        required: true
    },
    released: {
        type: String,
        required: true
    },
}, {
    timestamps: true
});
drug.index({ '$**': 'text' });
module.exports = db.model('drug', drug);
