module.exports = {
    CODE: {SUCCESS: 200, NOT_FOUND: 404, SERVER_ERROR: 500, BAD_REQUEST: 400, UNAUTHORIZED: 401, ALREADY_EXISTS: 422},
    RESPONSE_MESSAGES: {
        SUCCESS: {
            DRUGS_LIST: "Drugs List",
        },
        FAIL: {
            REQUEST_FAIL: 'Unable to complete the request.',
        },
    },
}
