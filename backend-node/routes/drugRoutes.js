const express = require('express');
const router = express.Router();
const groupController = require('../controllers/drugController');

router.get('/getAll', groupController.getAll);

module.exports = router;
