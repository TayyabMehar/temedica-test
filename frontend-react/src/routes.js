import React from 'react';

// Imports ================================================
// ========================================================
const ShoppingList = React.lazy(() => import('./components/Home/Home'));


const routes = [
    { path: '/shopping-list', exact: true, name: 'Shopping List', component: ShoppingList },
];

export default routes;
