import {takeLatest, put} from 'redux-saga/effects';
import types from '../actions/types';
import Api from '../lib/api';
import {api_urls} from '../lib/urls';
import {DrugDataType} from "../../components/Home/itemsList";

// get all items
export function* getAllDrugs() {
  yield takeLatest(types.GET_ALL_DRUGS_REQUEST, getAllDrugsApi);
}
function* getAllDrugsApi(params: any) {
  let param = params.payload;
  try {
    let response: DrugDataType[] = yield Api.get(
      `${api_urls.GET_ALL_DRUGS}?filter=${param}`,
    );
    yield put({type: types.GET_ALL_DRUGS_SUCCESS, payload: response});
  } catch (error) {
    yield put({type: types.GET_ALL_DRUGS_FAILURE, error: error});
  }
}
