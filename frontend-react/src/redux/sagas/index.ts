//Dependencies imports Redux Sagas
import {all, fork} from 'redux-saga/effects';
import * as drugsSaga from './drugsSaga';

// Redux Saga: Root Saga
export function* rootSaga() {
  yield all([fork(drugsSaga.getAllDrugs)]);
}
