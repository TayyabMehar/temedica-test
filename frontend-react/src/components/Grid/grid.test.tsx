import renderer from 'react-test-renderer';
import Grid from './grid';

test('Section is fine', () => {
    const tree = renderer
        .create(<Grid.Section />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('Row is fine', () => {
    const tree = renderer
        .create(<Grid.Row />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('LeftDiv is fine', () => {
    const tree = renderer
        .create(<Grid.LeftDiv />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('CenterDiv is fine', () => {
    const tree = renderer
        .create(<Grid.CenterDiv />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('RightDiv is fine', () => {
    const tree = renderer
        .create(<Grid.RightDiv />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
