import styled, {css} from 'styled-components';
import {Colors} from '../../common';

type RowPropType = {
    border?: boolean
}

const Section = styled.section.attrs({
    className: "container"
})`
    padding: 1em;
`;

const Row = styled.div<RowPropType>`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  ${props => props.border && css`
    padding: 5px;
    border: 1px solid ${Colors.black};
  `}
`;

type DivProp = {
    align?: string;
    flex?: number;
}

const LeftDiv = styled.div<DivProp>`
  display: flex;
  flex-direction: column;
  ${props => props.flex && css`
    flex: ${props.flex};
  `}
`;

const CenterDiv = styled.div<DivProp>`
  display: flex;
  flex-direction: column;
  ${props => props.flex && css`
    flex: ${props.flex};
  `}
`;

const RightDiv = styled.div<DivProp>`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  ${props => props.flex && css`
    flex: ${props.flex};
  `}
`;

const Grid =  {Section, Row, LeftDiv, CenterDiv, RightDiv};
export default Grid;
