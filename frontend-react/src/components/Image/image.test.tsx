import React from 'react';
import renderer from 'react-test-renderer';
import Image from './image';
import {Images} from "../../common";

test('Image is shown', () => {
    const tree = renderer
        .create(<Image src={Images.logo_image} />)
            .toJSON();
    expect(tree).toMatchSnapshot();
});
