import styled, {css} from 'styled-components';
import {Colors} from '../../common';

export type ImageProps = {
    width?: number;
    height?: number;
}
const Image = styled.img<ImageProps>`
  border-radius: 100px;
  ${props => css`
    max-width: ${props.width || 100}px;
    max-height: ${props.height || 100}px;
    color: ${Colors.black};
  `}
`;

export default Image;
