import React from 'react';
import renderer from 'react-test-renderer';
import Link from './Link';

it('Link renders correctly', () => {
    const tree = renderer
        .create(<Link href="http://www.facebook.com">Facebook</Link>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
