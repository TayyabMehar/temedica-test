import styled, {css} from 'styled-components';
import Grid from "../Grid/grid";
import TextElements from "../TextElements/textElements";
import {Colors} from '../../common';
import {useTranslation} from "react-i18next";

export type DrugDataType = {
    id?: string;
    _id?: string;
    diseases: string[];
    description: string;
    name: string;
    released: string;
};

const ScrollableDiv = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow-y: auto; 
  height: calc(100vh - 270px);;
`;

type ItemListPropType = {
    drugsList: DrugDataType[]
}

const ItemDiv = styled.div`
  padding: 10px 15px;
  margin: 5px 0px;
  ${() => css`
    border: 1px solid ${Colors.lightGrey};
  `}
`;

const ItemsList = (props: ItemListPropType) => {
    const [t] = useTranslation();
    const {drugsList} = props;

    if(!drugsList || drugsList?.length === 0) return <></>;
    return (
            <>
                <TextElements.H1 padding={'30px 0px 0px 0px'}>{t('SHOWING_X_RESULTS', {value: drugsList.length})}</TextElements.H1>
                <ScrollableDiv>
                    {drugsList?.map((drug: DrugDataType) => {
                        return (
                            <ItemDiv key={'item_' + (drug.id || drug._id)}>
                                <Grid.Row>
                                    <Grid.LeftDiv>
                                        <TextElements.H1>{drug.name}</TextElements.H1>
                                    </Grid.LeftDiv>
                                    <Grid.RightDiv>
                                        <TextElements.H1>{drug.released}</TextElements.H1>
                                    </Grid.RightDiv>
                                </Grid.Row>
                                {drug?.diseases?.map((disease, index) =>
                                    <TextElements.Span  key={'item_' + index}>{disease}</TextElements.Span>
                                )}
                                <TextElements.Paragraph padding={'10px 0px 0px 0px'}>{drug.description}</TextElements.Paragraph>
                            </ItemDiv>
                        )
                    })}
                </ScrollableDiv>
            </>
    );
}
export default ItemsList;
