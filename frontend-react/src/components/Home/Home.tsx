import React, {useEffect, useState} from 'react';
import data from "./dataset.json";
import Image from "../Image/image";
import Link from "../Link/Link";
import SearchBar from "../SearchBar/search";
import ItemsList, {DrugDataType} from "./itemsList";
import Grid from "../Grid/grid";
import TextElements from "../TextElements/textElements";
import {Images} from '../../common';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector, RootStateOrAny } from 'react-redux';
import {getDrugsList} from "../../redux/actions/drugActions";
import {DrugReducerType} from "../../redux/reducers/drugsReducer";

export const filteredDataFunc = (data: {drugs: DrugDataType[]}, text: string) => {
    return data.drugs.filter(
        (obj: DrugDataType) =>
            obj?.name.toLowerCase().includes(text) ||
            obj?.description.toLowerCase().includes(text) ||
            obj?.diseases.filter(disease =>
                disease.toLowerCase().includes(text),
            ).length > 0,
    );
}

const Home = () => {
    const dispatch = useDispatch();
    const drugsData = useSelector((state: RootStateOrAny) => state.drugs);
    const [t] = useTranslation();
    const [drugsList, setDrugsList] = useState<DrugDataType[]>([]);
    const [searchVal, setSearchVal] = useState('');

    useEffect(() => {
        setFilteredData(drugsData);
    },[drugsData]);

    const updateSearchValue = async (text: string) => {
        setSearchVal(text);
        if (text.length === 0 && drugsList.length > 0) setDrugsList([]);
        else if (text.length >= 2) dispatch(getDrugsList(text));
    }

    const setFilteredData = async (drugsData: DrugReducerType) => {
        let filteredData: DrugDataType[] = [];
        if(drugsData?.status && drugsData?.data) filteredData = drugsData.data;
        else if(drugsData?.error || drugsData?.status === false) filteredData = filteredDataFunc(data, searchVal);
        setDrugsList(filteredData);
    }

    return (
        <Grid.Section>
            <Grid.Row>
                <Grid.LeftDiv>
                    <Image src={Images.logo_image} alt='log_image' width={80} height={80} />
                </Grid.LeftDiv>
                <Grid.RightDiv>
                    <Link href={'https://google.com'} target={'_blank'}>{t('EXTERNAL_LINK')}</Link>
                </Grid.RightDiv>
            </Grid.Row>

            <TextElements.H1 padding={'30px 0px 0px 0px'}>{t('SEARCH')}</TextElements.H1>
            <SearchBar searchVal={searchVal} setSearchVal={updateSearchValue} />

            <ItemsList drugsList={drugsList}/>
        </Grid.Section>
    );
}
export default Home;
