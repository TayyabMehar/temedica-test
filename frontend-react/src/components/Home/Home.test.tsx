import {filteredDataFunc} from './Home';
import data from "./dataset.json";

test('Filter Data Function', () => {
    const value = filteredDataFunc(data, 'audi');
    expect(value.length).toBe(3);
});
