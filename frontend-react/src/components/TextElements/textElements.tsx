import styled, {css} from 'styled-components';
import {Colors, Constants} from '../../common';

type ParagraphPropType = {
    align?: string;
    padding?: string;
    size?: number;
}

type SpanPropType = {
    size?: number;
}

const Paragraph = styled.p<ParagraphPropType>`
  ${props => css`
    padding: ${props.padding || 0};
    text-align: ${props.align || 'left'};
    font-size: ${props.size || Constants.fontSize.small}px;
    text-align: justify;
  `}
`;

const Span = styled.span<SpanPropType>`
  display: block;
  ${props => css`
    font-size: ${props.size || Constants.fontSize.small}px;
  `}
`;

type HeadingProps = {
    size?: number;
    color?: string;
    padding?: string;
}
const H1 = styled.h1<HeadingProps>`
  ${props => css`
    padding: ${props.padding || 0};
    font-size: ${props.size || Constants.fontSize.large}px;
    color: ${props.color || Colors.black};
  `}
`;

const TextElements = {Paragraph, Span, H1}

export default TextElements;
