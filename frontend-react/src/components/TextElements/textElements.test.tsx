import renderer from 'react-test-renderer';
import TextElements from './textElements';

test('Paragraph is fine', () => {
    const tree = renderer
        .create(<TextElements.Paragraph />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('Span is fine', () => {
    const tree = renderer
        .create(<TextElements.Span />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});

test('H1 is fine', () => {
    const tree = renderer
        .create(<TextElements.H1 />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});
