import styled from 'styled-components';
import SearchIcon from '@material-ui/icons/Search';
import CloseOutlined from '@material-ui/icons/CloseOutlined';
import {useTranslation} from "react-i18next";
import {Constants} from '../../common';
import Grid from '../Grid/grid'

const Input = styled.input.attrs(props => ({
    type: "text",
    size: props.size || Constants.fontSize.small,
}))`
  padding: 0px 5px;
  border: none;
  outline: none;
  &:focus{
    border: none; 
  }
`;

type SearchPropType = {
    searchVal: string;
    setSearchVal: (str: string) => void;
}

const SearchBar = ({searchVal, setSearchVal}: SearchPropType) => {
    const [t] = useTranslation();

    const onInputChange = (event: { target: HTMLInputElement }) => {
        setSearchVal(event.target.value);
    }

    const clearSearch = () => {
        setSearchVal('');
    }

    return (
        <Grid.Row border>
            <Grid.LeftDiv>
                <SearchIcon />
            </Grid.LeftDiv>
            <Grid.CenterDiv flex={1}>
                <Input value={searchVal} placeholder={t('SEARCH_TERM')} onChange={onInputChange}/>
            </Grid.CenterDiv>
            <Grid.RightDiv>
                <CloseOutlined cursor={'pointer'} onClick={clearSearch} />
            </Grid.RightDiv>
        </Grid.Row>
    );
}
export default SearchBar;
