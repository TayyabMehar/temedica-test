import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import translations from './assets/translations.json';
import {Constants} from './common';

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .use(LanguageDetector)
    .init({
        resources: {...translations},
        lng: Constants.defaultLocale,
        fallbackLng: Constants.defaultLocale,

        interpolation: {
            escapeValue: false,
        },
    });
