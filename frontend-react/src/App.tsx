import React, {lazy, Suspense, Component} from 'react';
import './I18n'
import {Route, Switch, Redirect} from 'react-router-dom';
import {Provider} from 'react-redux';
import {store} from './redux/store';

import TextElements from './components/TextElements/textElements';
const Home = lazy(() => import(`./components/Home/Home`));

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Suspense fallback={<TextElements.Paragraph>Loading</TextElements.Paragraph>}>
                    <>
                        <Switch>
                            <Route exact path='/' component={Home}/>
                            <Redirect to='/' />
                        </Switch>
                    </>
                </Suspense>
            </Provider>
        );
    }
}
export default App;
