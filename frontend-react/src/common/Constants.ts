const Constants = {
  fontSize: {
    sTiny: 10,
    tinyM: 12,
    tiny: 13,
    small: 14,
    normal: 16,
    medium: 18,
    large: 20,
    xLarge: 22,
    huge: 30,
  },

  defaultLocale: 'en',
};
export default Constants;
