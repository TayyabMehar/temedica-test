import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import App from './App'

test('App is working', () => {
  const value = render(<BrowserRouter><App /></BrowserRouter>);
  expect(value).toBeTruthy();
});
